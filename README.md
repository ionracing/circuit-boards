# README #
This repo contains PCB files, schematic files, BOM's, Component libraries, footprints.
Orcad have been used for all the boards in this repo.

### What is this repository for? ###

Backup and sharing of the PCB projects for ION-Racing 2015/2016

### How do I get set up? ###
* Open the project you want to edit.
* Include the library in your program path.


### Who do I talk to? ###

* Aleksander K. Ferkingstad